#!/usr/bin/python3
#    Get the closest possible git source code to a released tarball
#
#    Copyright (c) 2018 Taylor Smock
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os,sys,git,pathlib
import cProfile
import threading
import time

def program_help(argv):
    sys.stdout.write("Usage: %s <repository> <foreign branch>\r\n" % (argv[0]))

def check_history(hash1, hash2):
    """
    Takes two hashes for comparison (hash1, hash2)
    If hash1 is the ancestor of hash2, we return 1
    If hash2 is the ancestor of hash1, we return 0
    If the hashes are NOT related, we return -1
    """
    repo = git.Repo(os.getcwd())
    if (repo.git.rev_list('-1', hash2 + ".." + hash1) != ""):
        return 1
    elif (repo.git.rev_list('-1', hash1 + ".." + hash2) != ""):
        return 0
    else:
        return -1

def history(FILE):
    """
    Get the full history of a file in the current git directory
    @return revisions of the file NOT in the current branch
    """
    repo = git.Repo(os.getcwd())
    currentbranch = repo.active_branch
    currentrevisions = repo.git.rev_list(str(currentbranch))
    currentrevisions = currentrevisions.split(os.linesep)
    revisions = repo.git.rev_list('--all', '--full-history',
            str(currentbranch), '--', FILE)
            #'--not', "--branches",
    revisions = revisions.split(os.linesep)
    revisions[:] = [r for r in revisions if r not in currentrevisions]
    return revisions

def size_difference(FILE, hash1):
    """
    Get the diff size between the FILE as it currently is and the hash1 of
    what it once was
    @return difference (lines added + lines removed)
    """
    repo = git.Repo(os.getcwd())
    # [added]   [removed]   FILE
    diff = repo.git.diff('--numstat', hash1, '--', FILE)
    diff = diff.split('\t')
    if (diff[0] == ''):
        return 0
    difference = int(diff[0]) + int(diff[1])
    return difference

def find_differences(check_file):
    """
    Get the hash with the smallest diff with check_file, not on current branch
    @return [hash with smallest diff, difference]
    """
    revisions = history(check_file)
    if (revisions is None or len(revisions) == 0 or revisions[0] == ''):
        return None
    else:
        hash_difference = [None, sys.maxsize]
        for check_hash in revisions:
            diff = size_difference(check_file, check_hash)
            if (diff < hash_difference[1]):
                hash_difference = [check_hash, diff]
                if (diff == 0):
                    break
            elif (diff == hash_difference[1]):
                hash_difference[0] = check_hash
        return hash_difference

def get_files(ignore_folders = [".git"]):
    """
    @input ignore_folders (optional) -- defaults to .git
    @return return_files A list of files not in ignore_folders
    """
    return_files = []
    for root, dirs, files in os.walk(os.getcwd(), topdown = True):
        dirs[:] = [d for d in dirs if d not in ignore_folders]
        for f in files:
            return_files.append(os.path.join(root, f).replace(os.getcwd() + os.sep, "", 1))
    return return_files

lock = threading.Lock()
def threaded_check(f, OUTPUTFILE):
    hash_difference = find_differences(f)
    if hash_difference is None:
        hash_difference = [None, -1]
    foutput = f + ": " + str(hash_difference)
    with lock:
        with open(OUTPUTFILE, "a") as output:
            output.write(foutput + os.linesep)
    print(foutput)
    return hash_difference

def compare(branch):
    """
    compare a branch against the rest of git history
    The branch may have no common ancestors with the rest of the git tree
    @return hashes that are close to the branch
    """
    repo = git.Repo(os.getcwd())
    if (repo.is_dirty()):
        sys.stderr.write("ERROR: Repository %s is dirty\r\n" % (os.getcwd()))
        raise

    try:
        repo.git.checkout(branch)
    except:
        sys.stderr.write("ERROR: Branch %s was not found\r\n" % (branch))
        raise
    files = get_files()
    hashes = []
    OUTPUTFILE = "output.log"
    lines = []
    if (not os.path.isfile(OUTPUTFILE)):
        with open(OUTPUTFILE, "w") as output:
            output.write("get_closest_kernel: v1" + os.linesep)
    with open(OUTPUTFILE, "r") as output:
        lines = output.readlines()
    lines[:] = [line.split(":")[0] for line in lines]

    for f in files:
        if (f in lines):
            print("Skipping %s" % (f))
            continue
        while (threading.active_count() > 4):
            time.sleep(10)
        #threaded_check(f)
        thread = threading.Thread(target=threaded_check, args = [f, OUTPUTFILE])
        thread.start()
    return 0

def chdir(repository):
    """
    Change directory into repository
    @input repository that ought to exist AND be a repository
    """
    os.chdir(repository)

def main(argv):
    try:
        if len(argv) < 3:
            program_help(argv)
            return 1
        if not os.path.exists(argv[1]):
            sys.stderr.write("ERROR: Repository %s was not found\r\n" % (argv[1]))
        else:
            os.chdir(argv[1])
        try:
            compare(argv[2])
        except Exception as e:
            import traceback
            traceback.print_exc()
            print(e)
            program_help(argv)
        return 0
    except KeyboardInterrupt:
        return -1

if __name__ == '__main__':
    DEBUG = True
    if DEBUG:
        cProfile.run('main(sys.argv)')
        sys.exit(0)
    else:
        sys.exit(main(sys.argv))
