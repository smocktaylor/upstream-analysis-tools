#!/usr/bin/python3
#    Get a sorted list of hashes from a log
#
#    Copyright (c) 2018 Taylor Smock
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
import os,sys,git
#import check_history from get-closest-tree.py

def check_history(hash1, hash2):
    """
    Takes two hashes for comparison (hash1, hash2)
    If hash1 is the ancestor of hash2, we return 1
    If hash2 is the ancestor of hash1, we return 0
    If the hashes are NOT related, we return -1
    """
    repo = git.Repo(os.getcwd())
    if (repo.git.rev_list('-1', hash2 + ".." + hash1) != ""):
        return 1
    elif (repo.git.rev_list('-1', hash1 + ".." + hash2) != ""):
        return 0
    else:
        return -1


def sort(hashes):
    """
    Sort the hashes (oldest first)
    @input: List of git hashes to sort
    @return: [sorted_hashes, True if we did not sort hashes]
    """
    not_changed = True
    sorted_hashes = []
    for hash1 in hashes:
        if hash1 in sorted_hashes:
            continue
        sorted_hashes.append(hash1)

    index = 0
    while index < len(sorted_hashes) - 2:
        if sorted_hashes[index] == sorted_hashes[index + 1]:
            sorted_hashes = sorted_hashes[:index - 1] + sorted_hashes[index + 1:]
            continue
        check = check_history(sorted_hashes[index], sorted_hashes[index + 1])
        """
        If 0 (index + 1 older than index) or -1 (not related) move back
        """
        if check == 0 or check == -1:
            tmp = sorted_hashes[index]
            sorted_hashes[index] = sorted_hashes[index + 1]
            sorted_hashes[index + 1] = tmp
        elif check == 1:
            pass
        index += 1

    if sorted_hashes != hashes:
        not_changed = False
    return [sorted_hashes, not_changed]

def checklog(OUTPUTLOG):
    """
    @input OUTPUTLOG to check for hashes
    @return unique hashes from oldest to newest
    """
    with open(OUTPUTLOG, "r") as log:
        lines = log.readlines()
    content = []
    for line in lines:
        aline = line.split(": ")[1].strip().strip("[]").split(",")[0].strip("'")
        if aline == "v1" or aline == "None" or aline is None:
            continue
        if aline not in content:
            content.append(aline)
    os.chdir(os.path.dirname(OUTPUTLOG))
    hashes = content
    hashcheck = False
    iterations = 0
    while not hashcheck and iterations < len(hashes):
        print("Running... (%s/%s)" % (iterations, len(hashes)))
        [hashes, hashcheck] = sort(content)
        iterations = iterations + 1
    print("Final version")
    for hash1 in hashes:
        print(hash1)

def main(argv):
    if len(argv) < 2:
        print("Need an output.log")
    else:
        checklog(argv[1])

if __name__ == '__main__':
   sys.exit(main(sys.argv))
